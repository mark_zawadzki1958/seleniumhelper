package com.mezawadzki.simpleSelenium;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Base Unit test for simple App.
 * TODO put JUnit tags in.
 */
public class AppTest
extends TestCase
{
	/**
	 * Create the test case
	 *
	 * @param testName name of the test case
	 */
	public AppTest( final String testName )
	{
		super( testName );
	}

	/**
	 * @return the suite of tests being tested
	 */
	public static Test suite()
	{
		return new TestSuite( AppTest.class );
	}

	/**
	 * Rigorous Test :-)
	 */
	public void testApp()
	{
		assertTrue( true );
	}
}
