package com.mezawadzki.simpleSelenium;

import java.util.List;

/**
 * Test data loader.
 * TODO work on this.
 * @author Mark
 *
 */
public class TestData {

	private String data;
	private List<String> dataList;


	/**
	 * you can use this to load the data from the
	 * @return the data
	 */
	public String getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(final String data) {
		this.data = data;
	}
	/**
	 * @return the dataList
	 */
	public List<String> getDataList() {
		return dataList;
	}
	/**
	 * @param dataList the dataList to set
	 */
	public void setDataList(final List<String> dataList) {
		this.dataList = dataList;
	}
}
