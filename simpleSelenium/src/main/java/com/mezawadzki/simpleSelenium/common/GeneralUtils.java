package com.mezawadzki.simpleSelenium.common;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class GeneralUtils {
	/**
	 * Logger for this class
	 */
	private static final Logger LOGGER = LogManager.getLogger(GeneralUtils.class.getName());

	private GeneralUtils() {

	}

	public static final boolean lookForWholeWord(final String string, final String wordToLookFor) {
		final String pattern = ".*\\b" + wordToLookFor + "\\b.*";
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("lookForWholeWord(String, String) - boolean pattern={}", pattern); //$NON-NLS-1$
		}

		final boolean contains = string.matches(pattern);
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("lookForWholeWord(String, String) - boolean contains={}", contains); //$NON-NLS-1$
		}

		return contains;
	}

	/**
	 * Wrap string in double quotes
	 * @param string
	 * @return quoted string
	 */
	public static final String quote(final String string) {
		return new StringBuilder().append('"').append(string).append('"').toString();
	}

	/**
	 * Remove all non-numeric characters from ...
	 * @param raw what is left should be a number, which is ...
	 * @return  a string consisting only of numeric characters.
	 */
	public static final String stripNumber(final String raw){
		return raw.replaceAll("[^\\d.]", "");
	}

}
