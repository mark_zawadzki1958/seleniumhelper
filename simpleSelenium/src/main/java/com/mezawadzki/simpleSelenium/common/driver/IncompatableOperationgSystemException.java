package com.mezawadzki.simpleSelenium.common.driver;

public class IncompatableOperationgSystemException extends RuntimeException {

	private static final long serialVersionUID = -7688765359234270920L;

	public IncompatableOperationgSystemException() {
	}

	public IncompatableOperationgSystemException(final String message) {
		super(message);
	}

	public IncompatableOperationgSystemException(final Throwable cause) {
		super(cause);
	}

	public IncompatableOperationgSystemException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public IncompatableOperationgSystemException(final String message, final Throwable cause, final boolean enableSuppression,
			final boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
