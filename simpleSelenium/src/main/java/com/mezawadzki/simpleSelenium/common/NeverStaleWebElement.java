package com.mezawadzki.simpleSelenium.common;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;

public class NeverStaleWebElement implements WebElement {
	/**
	 * Logger for this class
	 */
	private static final Logger LOGGER = LogManager.getLogger(NeverStaleWebElement.class.getName());

	private WebElement element;
	private final WebDriver driver;
	private final By foundBy;

	public NeverStaleWebElement(final WebElement element, final WebDriver driver, final By foundBy) {
		this.element = element;
		this.driver = driver;
		this.foundBy = foundBy;
	}

	@Override
	public void click() {
		try {
			element.click();
		} catch (final StaleElementReferenceException e) {
			if (LOGGER.isTraceEnabled()) {
				LOGGER.trace("retry click() after exception. - e={}", e); //$NON-NLS-1$
			}

			e.printStackTrace();
			// assumes implicit wait, use custom findElement() methods for
			// custom behaviour
			element = driver.findElement(foundBy);

			// recursion, consider a conditioned loop instead
			click();
		}
	}

	@Override
	public <X> X getScreenshotAs(final OutputType<X> target) throws WebDriverException {
		// Don't want to kepp taking screeenshots.
		return null;
	}

	@Override
	public void clear() {
		// don't clear cause we are going to keep trying.

	}

	@Override
	public WebElement findElement(final By by) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<WebElement> findElements(final By by) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getAttribute(final String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getCssValue(final String propertyName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Point getLocation() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Rectangle getRect() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Dimension getSize() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getTagName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getText() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isSelected() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void sendKeys(final CharSequence... keysToSend) {
		// TODO Auto-generated method stub

	}

	@Override
	public void submit() {
		// TODO Auto-generated method stub

	}

	// ... similar for other methods, too

}
