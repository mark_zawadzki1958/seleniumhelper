package com.mezawadzki.simpleSelenium.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.logging.log4j.core.config.ConfigurationSource;
import org.apache.logging.log4j.core.config.Configurator;

public class Log4JInitializer {

	private static final String CLASS_DIR = System.getProperty("user.dir") + File.separator + "class" + File.separator;

	public static void initialize() throws FileNotFoundException, IOException {

		System.setProperty("java.util.logging.manager", "org.apache.logging.log4j.jul.LogManager");
		final String log4jConfigFile = CLASS_DIR + "log4j2.xml";
		final String julLogFile = CLASS_DIR + "logging.properties";
		System.setProperty("java.util.logging.config.file=", julLogFile);
		System.out.println(log4jConfigFile);
		final ConfigurationSource source = new ConfigurationSource(new FileInputStream(log4jConfigFile));
		Configurator.initialize(null, source);
	}
}
