package com.mezawadzki.simpleSelenium.common.transform;

public interface Transformer {

	/**
	 * normalize string, for example, normalize date strings, phone numbers, etc. to a given string format.
	 * @param t
	 * @return
	 */
	public String normalize(String string);
}
