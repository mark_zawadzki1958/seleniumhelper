package com.mezawadzki.simpleSelenium.common;

import org.openqa.selenium.WebDriverException;

public class MaxRetryException extends RuntimeException {

	private static final long serialVersionUID = -4409315711080671769L;

	public MaxRetryException(final String string, final WebDriverException e) {
		super(string, e);
	}

}
