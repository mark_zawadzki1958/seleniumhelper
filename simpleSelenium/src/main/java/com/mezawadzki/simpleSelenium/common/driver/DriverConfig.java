package com.mezawadzki.simpleSelenium.common.driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public interface DriverConfig {

	public static final String FIREFOX_DRIVER_PATH = "drivers\\geckodriver.exe";
	public static final String CHROME_DRIVER_PATH = "drivers\\chromedriver.exe";
	/** ie 32 bit driver */
	public static final String IE_DRIVER_PATH_32 = "drivers\\winx32\\IEDriverServer.exe";
	/** ie 64 bit driver */
	public static final String IE_DRIVER_PATH_64 = "drivers\\winx64\\IEDriverServer.exe";

	public static final String OPERA_DRIVER_PATH_WIN32 = "drivers\\operadriver_win32\\operadriver.exe";
	public static final String OPERA_DRIVER_PATH_WIN64 = "drivers\\operadriver_win64\\operadriver.exe";
	public static final String OPERA_DRIVER_PATH_LINUX = "drivers\\operadriver_linux64\\operadriver.exe";
	public static final String OPERA_DRIVER_PATH_MAC64 = "drivers\\operadriver_mac64\\operadriver.exe";

	public static final String PHANTHOMJS_DRIVER_PATH_WIN64 = "drivers\\phantomjs.exe";

	/**
	 * Safari Driver .
	 */
	public static final String SAFIRI_DRIVER_PATH = "drivers\\SafariDriver.safariextz";


	public static final String WEBDRIVER_GECKO_DRIVER = "webdriver.gecko.driver";
	public static final String WEBDRIVER_CHROME_DRIVER = "webdriver.chrome.driver";
	public static final String WEBDRIVER_IE_DRIVER = "webdriver.ie.driver";
	public static final String WEBDRIVER_SAFARI_DRIVER = "webdriver.safari.driver";
	public static final String WEBDRIVER_OPERA_DRIVER = "webdriver.opera.driver";
	public static final String WEBDRIVER_PHANTHOMJS_DRIVER = "phantomjs.binary.path";

	WebDriver getWebDriverObject(DesiredCapabilities desiredCapabilities);

	DesiredCapabilities getDesiredCapabilities();
}