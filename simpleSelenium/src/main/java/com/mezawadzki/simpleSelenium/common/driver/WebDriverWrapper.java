package com.mezawadzki.simpleSelenium.common.driver;

import static com.mezawadzki.simpleSelenium.common.driver.DriverType.FIREFOX;
import static com.mezawadzki.simpleSelenium.common.driver.DriverType.valueOf;

import java.net.MalformedURLException;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.mezawadzki.simpleSelenium.common.PlatformDetection;

public final class WebDriverWrapper {

	private final PlatformDetection platformDetection = PlatformDetection.getInstance();
	private WebDriver webdriver;
	private DriverType selectedDriverType;

	private final DriverType defaultDriverType = FIREFOX;
	/** set the system property <i>browser</i> to browser under test. Defaults to
	 *   {@link com.mezawadzki.simpleSelenium.common.driver.DriverType.FIREFOX}
	 * @see {@link com.mezawadzki.simpleSelenium.common.driver.DriverType} for other valid browsers.
	 */
	private final String rawBrowser =  System.getProperty("browser");
	private final String browser = StringUtils.isBlank(rawBrowser) ? "" : rawBrowser.toUpperCase();
	private final String operatingSystem = platformDetection.getOs();
	public final String systemArchitecture = platformDetection.getArch();


	public WebDriver getDriver() throws Exception {
		if (null == webdriver) {
			selectedDriverType = determineEffectiveDriverType();
			final DesiredCapabilities desiredCapabilities = selectedDriverType.getDesiredCapabilities();
			instantiateWebDriver(desiredCapabilities);
		}
		return webdriver;
	}

	public void quitDriver() {

		if (null != webdriver) {
			webdriver.quit();
			webdriver = null;
		}
	}

	private DriverType determineEffectiveDriverType() {
		DriverType driverType = defaultDriverType;
		try {
			driverType = valueOf(browser);
		} catch (final IllegalArgumentException ignored) {
			System.err.println("Unknown driver specified, defaulting to '" + driverType + "'...");
		} catch (final NullPointerException ignored) {
			System.err.println("No driver specified, defaulting to '" + driverType + "'...");
		}
		return driverType;
	}

	private void instantiateWebDriver(final DesiredCapabilities desiredCapabilities) throws MalformedURLException {
		System.out.println(" ");
		System.out.println("Current Operating System: " + operatingSystem);
		System.out.println("Current Architecture: " + systemArchitecture);
		System.out.println("Current Browser Selection: " + selectedDriverType);
		System.out.println(" ");
		webdriver = selectedDriverType.getWebDriverObject(desiredCapabilities);
	}
}