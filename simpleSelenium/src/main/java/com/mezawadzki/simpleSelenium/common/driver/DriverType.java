package com.mezawadzki.simpleSelenium.common.driver;

import java.util.Arrays;
import java.util.HashMap;
import java.util.logging.Level;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;

import com.mezawadzki.simpleSelenium.common.Configuration;
import com.mezawadzki.simpleSelenium.common.PlatformDetection;

@SuppressWarnings("unused") // remove when all browsers are supported.
/** encapsulates browser specific preferences and capabilities */
public enum DriverType implements DriverConfig {
	FIREFOX {
		@Override
		public DesiredCapabilities getDesiredCapabilities() {
			final DesiredCapabilities capabilities = DesiredCapabilities.firefox();

			final FirefoxProfile profile = new FirefoxProfile();
			profile.setPreference("browser.download.folderList", 2);
			profile.setPreference("browser.helperApps.alwaysAsk.force", false);
			profile.setPreference("browser.download.manager.showWhenStarting", false);
			profile.setPreference("browser.helperApps.neverAsk.saveToDisk",
					"application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,text/csv");
			profile.setPreference("marionette.logging", "TRACE");
			profile.setPreference("auto.update.enabled", false);

			final LoggingPreferences logPreferences = new LoggingPreferences();
			logPreferences.enable(LogType.BROWSER, Level.OFF);
			logPreferences.enable(LogType.CLIENT, Level.OFF);
			logPreferences.enable(LogType.DRIVER, Level.OFF);
			logPreferences.enable(LogType.PERFORMANCE, Level.OFF);
			logPreferences.enable(LogType.PROFILER, Level.OFF);
			logPreferences.enable(LogType.SERVER, Level.OFF);
			capabilities.setCapability(CapabilityType.LOGGING_PREFS, logPreferences);

			capabilities.setCapability(FirefoxDriver.PROFILE, profile);

			return capabilities;
		}

		@Override
		public WebDriver getWebDriverObject(final DesiredCapabilities capabilities) {
			System.setProperty(WEBDRIVER_GECKO_DRIVER, FIREFOX_DRIVER_PATH);
			/** TODO fix deprecation -. Replaced by  FirefoxDriver(OPtions options)*/
			return new FirefoxDriver(capabilities);
		}
	},
	CHROME {
		@Override
		public DesiredCapabilities getDesiredCapabilities() {
			final DesiredCapabilities capabilities = DesiredCapabilities.chrome();

			final HashMap<String, Object> chromePreferences = new HashMap<String, Object>();
			chromePreferences.put("profile.default_content_settings.popups", 0);
			chromePreferences.put("download.default_directory", Configuration.DOWNLOAD_LOCATION);
			chromePreferences.put("pdfjs.disabled", true);
			chromePreferences.put("profile.password_manager_enabled", "false");

			final ChromeOptions options = new ChromeOptions();
			final HashMap<String, Object> chromeOptionsMap = new HashMap<String, Object>();
			options.setExperimentalOption("prefs", chromePreferences);
			options.addArguments("--test-type");

			capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptionsMap);
			capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			capabilities.setCapability(ChromeOptions.CAPABILITY, options);
			capabilities.setCapability("chrome.switches", Arrays.asList("--no-default-browser-check"));
			capabilities.setCapability("chrome.prefs", chromePreferences);

			return capabilities;
		}

		@Override
		public WebDriver getWebDriverObject(final DesiredCapabilities capabilities) {
			System.setProperty(WEBDRIVER_CHROME_DRIVER, CHROME_DRIVER_PATH);

			/** TODO fix deprecation -. Replaced by  ChromeDriver(OPtions options)*/
			return new ChromeDriver(capabilities);
		}
	},
	IE {
		private final PlatformDetection platformDetection = PlatformDetection.getInstance();

		@Override
		public DesiredCapabilities getDesiredCapabilities() {
			final DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
			capabilities.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);
			capabilities.setCapability(InternetExplorerDriver.ENABLE_PERSISTENT_HOVERING, true);
			capabilities.setCapability("requireWindowFocus", true);
			return capabilities;
		}

		@Override
		public WebDriver getWebDriverObject(final DesiredCapabilities capabilities) {

			// TODO IE is not connection with the LDAp correctly (???). Tried
			// everything that I could google on the problem.
			// see for IE,
			// https://www.joecolantonio.com/2013/08/01/selenium-webdriver-fix-for-3-common-ie-errors/
			// if you get an error here. This might help or it might not.
			// Right now nothing is working for me.
			// remove the UnsupportedOperationException when fixed
			if (true) {
				throw new UnsupportedOperationException();
			}

			if (!platformDetection.getOs().equals(PlatformDetection.OS_WINDOWS)) {
				throw new IncompatableOperationgSystemException("Must run on Wimdows.");
			}

			String driverPath = "";

			final String platform = platformDetection.getArch();
			if (PlatformDetection.ARCH_X86_32.equals(platform)) {
				driverPath = IE_DRIVER_PATH_32;
			} else {
				if (PlatformDetection.ARCH_X86_64.equals(platform)) {
					driverPath = IE_DRIVER_PATH_64;
				} else {
					throw new IncompatableArchitectureException("only 32 and 64 bit architectures.");
				}
			}
			System.setProperty(WEBDRIVER_IE_DRIVER, driverPath);

			return new InternetExplorerDriver(capabilities);
		}
	},
	SAFARI {
		@Override
		public DesiredCapabilities getDesiredCapabilities() {
			final DesiredCapabilities capabilities = DesiredCapabilities.safari();
			capabilities.setCapability("safari.cleanSession", true);
			return capabilities;
		}

		@Override
		public WebDriver getWebDriverObject(final DesiredCapabilities capabilities) {
			if (!PlatformDetection.OS_OSX.equals(PlatformDetection.getInstance().getOs())) {
				throw new UnsupportedOperationException("Must run on Mac.");
			}
			System.setProperty(WEBDRIVER_SAFARI_DRIVER, SAFIRI_DRIVER_PATH);
			return new SafariDriver(capabilities);
		}
	},
	OPERA {
		@Override
		public DesiredCapabilities getDesiredCapabilities() {
			final DesiredCapabilities capabilities = DesiredCapabilities.operaBlink();
			return capabilities;
		}

		@Override
		public WebDriver getWebDriverObject(final DesiredCapabilities capabilities) {
			final PlatformDetection platformDetection = PlatformDetection.getInstance();

			final String os = platformDetection.getOs();
			switch (os) {
			case PlatformDetection.OS_LINUX:
				System.setProperty(WEBDRIVER_OPERA_DRIVER, OPERA_DRIVER_PATH_LINUX);
				break;
			case PlatformDetection.OS_OSX:
				System.setProperty(WEBDRIVER_OPERA_DRIVER, OPERA_DRIVER_PATH_MAC64);
				break;

			case PlatformDetection.OS_WINDOWS:
				final String arch = platformDetection.getArch();
				switch (arch) {
				case PlatformDetection.ARCH_X86_32:
					System.setProperty(WEBDRIVER_OPERA_DRIVER, OPERA_DRIVER_PATH_WIN32);
					break;
				case PlatformDetection.ARCH_X86_64:
					System.setProperty(WEBDRIVER_OPERA_DRIVER, OPERA_DRIVER_PATH_WIN64);
					break;
				default:
					throw new IncompatableArchitectureException("Windows OS - " + arch + " was not expected");

				}
				break;
			default:
				throw new IncompatableOperationgSystemException("Not a supported O/S " + os);
			}
			/** TODO fix deprecation -. Replaced by  OperaDriver(OPtions options)*/
			return new OperaDriver(capabilities);
		}
	}
	/*
	 * TODO fix maven repo problem.
	,
	PHANTHOMJS {
		// TODO - test this!!!
		public DesiredCapabilities getDesiredCapabilities() {
			DesiredCapabilities capabilities = DesiredCapabilities.phantomjs();
			capabilities.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,
					PHANTHOMJS_DRIVER_PATH_WIN64);
			capabilities = new DesiredCapabilities();
			capabilities.setJavascriptEnabled(true);
			capabilities.setCapability(PhantomJSDriverService.PHANTOMJS_PAGE_SETTINGS_PREFIX, "Y");
			capabilities.setCapability("phantomjs.page.settings.userAgent",
					"Mozilla/5.0 (Windows NT 10.0; WOW64; rv:54.0) Gecko/20100101 Firefox/54.0");
			capabilities.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, new String[] { "--web-security=false",
					"--ssl-protocol=any", "--ignore-ssl-errors=true", "--webdriver-loglevel=DEBUG" });
			return capabilities;
		}

		public WebDriver getWebDriverObject(DesiredCapabilities capabilities) {
			System.setProperty(WEBDRIVER_PHANTHOMJS_DRIVER, PHANTHOMJS_DRIVER_PATH_WIN64);
			return new PhantomJSDriver(capabilities);
		}
	}
	 */
}