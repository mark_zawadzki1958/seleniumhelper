package com.mezawadzki.simpleSelenium.common;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.rules.TestName;
import org.junit.runner.Description;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
/**
 * Global Actions to take on fail. Log and take screenshot.
 * @author Mark
 *
 */
public final class OnFailureAction extends TestName {

	/** Folder in which to store reports/logs.
	 * Determined by system property <i>REPORT_FOLDER</i>
	 * */
	public static final String REPORT_FOLDER = reportsFolderLocation();
	/**
	 * Folder to store screen captures.
	 * Determined by system property <i>REPORT_FOLDER</i> and browser type.</br>
	 * Of form {REPORT_FOLDER}/{Browser_Type}/captures <br/>
	 * &nbsp &nbsp  where report folder is determined by the system property REPORT_FOLDER and browser is determined by the <i>browser</i> system property.<br/>
	 * Default browser is FIREFOX<br/>
	 * Default report folder is "reports".<br>
	 * Captures can be found in "captures".<be/>
	 */
	public static final String CAPTURE_DIR = REPORT_FOLDER + '/' + determineBrowser() + "/captures/";

	private static String determineBrowser() {
		return StringUtils.isBlank(System.getProperty("browser")) ? "Firefox"
				: StringUtils.capitalize(System.getProperty("browser"));
	}

	private static WebDriver driver = null;
	/**
	 * The reports dir location.
	 *
	 * @return The reports dir location.
	 */
	public final static String reportsFolderLocation() {
		return StringUtils.isBlank(System.getProperty("REPORT_FOLDER")) ? "../reports/" : System.getProperty("REPORT_FOLDER");
	}


	/**
	 * Logger for this class
	 */
	private static final Logger LOGGER = LogManager.getLogger(OnFailureAction.class.getName());

	@Override
	protected void failed(final Throwable e, final Description description) {
		LOGGER.debug("Test failed. Creating screen shot for " + description.getDisplayName());

		captureScreenshot(description.getDisplayName());

	}

	private void captureScreenshot(final String description) {
		String fileName = "";
		if (StringUtils.isBlank(description)) {
			fileName = "NO_DESCRIPTION-" + Long.toString(new Date().getTime());
		} else {
			fileName = createTemporalFileName(description);
		}

		try {
			new File(CAPTURE_DIR).mkdirs(); // Insure Folder is there
			final String filepath = CAPTURE_DIR + fileName + ".png";
			final FileOutputStream out = new FileOutputStream(filepath);
			out.write(((TakesScreenshot) getDriver()).getScreenshotAs(OutputType.BYTES));
			out.close();
			LOGGER.debug("Screen shot saved to " + filepath);
		} catch (final Exception e) {
			// No need to crash the com.mezawadzki.bdd.tests if the screenshot fails
		}
	}

	private String createTemporalFileName(final String description) {
		final String timeStamp = new SimpleDateFormat("yyyyMMdd-kkmmss").format(new Date());

		return description + '-' + timeStamp;
	}

	public static WebDriver getDriver() {
		return driver;
	}

	public static void setDriver(final WebDriver driver) {
		OnFailureAction.driver = driver;
	}
}
