package com.mezawadzki.simpleSelenium.common;

import java.io.File;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.mezawadzki.simpleSelenium.common.driver.UnsupportedOperationgSystemException;

public interface Configuration {

	Logger LOGGER = LogManager.getLogger(Configuration.class.getName());

	/** developer's adva-net server URL */
	String LOCAL_URL = ">URL<";

	/** WIndows family of O/S */
	String WINDOWS = "WINDOWS";

	/** Linux family of O/S */
	String LINUX = "LINUX";

	/** Apple MacIntosh family of O/S */
	String MACINTOSH = "MAC";
	/**
	 * URL of the app to be tested.
	 */
	String START_URL = StringUtils.isBlank(System.getProperty("URL")) ? LOCAL_URL : System.getProperty("URL");

	/** Where download from the browser end up for WinDoze. */
	String HOME = System.getProperty("user.home");
	String WINDOW_DOWNLOAD_FOLDER = HOME + File.separator + "Downloads" + File.separator;

	/** Where download from the browser end up for Solaris and Linux. */
	String NUX_DOWNLOAD_FOLDER = "/downloads";
	// TODO MAC

	/** Where download from the browser end up for Mac. */
	String MAC_DOWNLOAD_FOLDER = "???";

	String DOWNLOAD_LOCATION = downloadLocation();

	/** Timeout interval seconds */
	long DEFAULT_TIMEOUT = 60;

	/** Polling interval seconds */
	long DEFAULT_POLLING_EVERY = 5;

	/** Timeout interval seconds */
	long SHORT_TIMEOUT = 10;

	/** Polling interval seconds */
	long SHORT_POLLING_EVERY = 1;

	int DEFAULT_MAX_RETRIES = 25;
	int SHORT_MAX_RETRIES = 10;

	String TEST_AUTH_PASSWORD = "";

	String TEST_AUTH_USERNAME = "";

	/**
	 * determine download location.
	 *
	 * @return download location
	 */
	public static String downloadLocation() {
		String result = null;
		if (!StringUtils.isBlank(System.getProperty("DOWNLOAD_FILDER"))) {
			result = System.getProperty("DOWNLOAD_FILDER");
		} else {
			final String os = PlatformDetection.getInstance().getOs();
			switch (os) {
			case PlatformDetection.OS_WINDOWS:
				result = WINDOW_DOWNLOAD_FOLDER;
				break;

			case PlatformDetection.OS_LINUX:
			case PlatformDetection.OS_SOLARIS:
				result = NUX_DOWNLOAD_FOLDER;
				break;
			case PlatformDetection.OS_OSX:
				result = MAC_DOWNLOAD_FOLDER;
			default:
				final Exception e = new UnsupportedOperationgSystemException(
						"O/S " + os + " not recognized in determinining downLoad folder.");
				LOGGER.fatal(e);

			}
		}
		return result;
	}

}