package com.mezawadzki.simpleSelenium.common;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.opera.OperaDriver;

/**
 * Automagiaclly detect browser.
 * @author Mark
 *
 */
public class BrowserDetection {
	/**
	 * Logger for this class
	 */
	private static final Logger LOGGER = LogManager.getLogger(BrowserDetection.class.getName());

	public static enum Browsers {
		UNKNOWN, CHROME, FIREFOX, OPERA, IE
	}

	public static Browsers getCurrentBrowserName(final WebDriver driver, Browsers browserName) {
		// lazy init.
		if (driver instanceof JavascriptExecutor && Browsers.UNKNOWN.equals(browserName)) {
			final JavascriptExecutor js = (JavascriptExecutor) driver;
			if (LOGGER.isInfoEnabled()) {
				LOGGER.info("getCurrentBrowserName() - JavascriptExecutor js={}", js); //$NON-NLS-1$
			}
			if (driver instanceof ChromeDriver) {
				browserName = Browsers.CHROME;
			} else if (driver instanceof FirefoxDriver) {
				browserName = Browsers.FIREFOX;
			} else if (driver instanceof OperaDriver) {
				browserName = Browsers.OPERA;
			}
		}
		return browserName;
	}

	/* private constructor for all static class */
	private BrowserDetection() {

	}

}