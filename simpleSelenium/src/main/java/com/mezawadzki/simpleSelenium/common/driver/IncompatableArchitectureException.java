package com.mezawadzki.simpleSelenium.common.driver;

public class IncompatableArchitectureException extends RuntimeException {

	private static final long serialVersionUID = 3476783274077061941L;

	public IncompatableArchitectureException() {
	}

	public IncompatableArchitectureException(final String message) {
		super(message);
	}

	public IncompatableArchitectureException(final Throwable cause) {
		super(cause);
	}

	public IncompatableArchitectureException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public IncompatableArchitectureException(final String message, final Throwable cause, final boolean enableSuppression,
			final boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
