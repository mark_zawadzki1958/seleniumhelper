package com.mezawadzki.simpleSelenium.common.driver;

import org.openqa.selenium.WebDriver;

public class DriverFactory {
	private static ThreadLocal<WebDriverWrapper> driverThread;

	public static void instantiateDriverObject() {
		driverThread = new ThreadLocal<WebDriverWrapper>() {
			@Override
			protected WebDriverWrapper initialValue() {
				final WebDriverWrapper webDriverThread = new WebDriverWrapper();
				return webDriverThread;
			}
		};
	}

	public static WebDriver getDriver() throws Exception {
		return driverThread.get().getDriver();
	}

	public static void quitDriver() throws Exception {
		driverThread.get().quitDriver();

	}

}