package com.mezawadzki.simpleSelenium.common;

import static com.mezawadzki.simpleSelenium.common.Configuration.START_URL;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.mezawadzki.simpleSelenium.common.driver.DriverFactory;
import com.mezawadzki.simpleSelenium.utils.JSWaiter;

public class AccessApplication {
	/**
	 * Logger for this class
	 */
	private static final Logger LOGGER = LogManager.getLogger(AccessApplication.class.getName());
	private static WebDriver driver = null;
	private static PlatformDetection platformDetection = PlatformDetection.getInstance();

	/**
	 * set up drivers, and login to app.
	 *
	 * @return
	 * @throws Exception
	 */
	public static WebDriver login() throws Exception {

		createDriver();
		OnFailureAction.setDriver(driver);
		JSWaiter.setDriver(driver);

		// Open the page we want to open
		driver.get(START_URL);

		try {
			final WebElement usernameEl = driver.findElement(By.name("username"));
			usernameEl.sendKeys(Configuration.TEST_AUTH_USERNAME);
			final WebElement passwordEl = driver.findElement(By.name("password"));
			passwordEl.sendKeys(Configuration.TEST_AUTH_PASSWORD);
			final WebElement loginButton = driver.findElement(By.xpath("/html/body/div/div[2]/form/button"));
			if (PlatformDetection.OS_WINDOWS.equals(platformDetection.getOs())) {
				//  IE click does not always work sendkeys() is more reliable
				loginButton.sendKeys(Keys.ENTER);
			} else {
				loginButton.click();
			}
		} catch (final Exception e) {
			LOGGER.error("login()", e); //$NON-NLS-1$
			throw e;
		}
		return driver;
	}

	public static WebDriver createDriver() throws Exception {
		if (driver == null) {
			DriverFactory.instantiateDriverObject();
			driver = DriverFactory.getDriver();
		}
		return driver;
	}

	public static void endTests() throws Exception{
		try { //kludge to avoid bug in selenium
			DriverFactory.quitDriver();
		} catch (final IllegalAccessError e) {
			LOGGER.error("endTests() - expected Selnium bug.", e); //$NON-NLS-1$
		}
		driver = null;

	}
	public static void logout() throws Exception  {
		try {
			final WebElement webEl = new WebDriverWait(driver, 5).until((final WebDriver dr1) -> dr1.findElement(By.id("//button[@id='logoutButton']")));
			webEl.click();
		} catch (final Exception e) {
			//LOGGER.warn("logout() - exception ignored", e); //$NON-NLS-1$

		}
		endTests();
	}
}
