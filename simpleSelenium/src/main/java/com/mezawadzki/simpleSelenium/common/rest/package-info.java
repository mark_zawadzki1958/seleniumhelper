/**
 * Classes to handle REST calls.
 */
/**
 * @author mzawadzki
 *
 */
package com.mezawadzki.simpleSelenium.common.rest;