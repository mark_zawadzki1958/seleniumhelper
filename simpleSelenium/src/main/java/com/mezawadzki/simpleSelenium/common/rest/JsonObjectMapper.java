package com.mezawadzki.simpleSelenium.common.rest;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonObjectMapper {

	private static ObjectMapper mapper;

	static {
		mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}

	/**
	 * Convert JSON to CLASS objects.
	 *
	 * @param json
	 *            JSON string
	 * @return List of {@link com.mezawadzki.bdd.so.CLASS.main.CLASS}
	 * @throws JsonParseException
	 *             {@link com.fasterxml.jackson.core.JsonParseException}
	 * @throws JsonMappingException{@link
	 * 			com.fasterxml.jackson.databind.JsonMappingException}
	 * @throws IOException
	 *             [@link java.io.IOException}
	 */
	public static List<CLASS> mapCLASSs(String json)
			throws JsonParseException, JsonMappingException, IOException {
		return mapper.reader().forType(new TypeReference<List<CLASS>>() {
		}).readValue(json);
	}

	}
