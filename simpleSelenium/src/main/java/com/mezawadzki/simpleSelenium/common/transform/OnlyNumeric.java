package com.mezawadzki.simpleSelenium.common.transform;

public class OnlyNumeric implements Transformer {

	/**
	 * @see com.mezawadzki.simpleSelenium.common.transform.Transformer#normalize(S)
	 * remove non-numeric characters, leaving only numeric.
	 */
	@Override
	public  String normalize(String string) {
		return string.replaceAll("[^\\d.]", "");
	}

}
