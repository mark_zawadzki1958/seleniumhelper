package com.mezawadzki.simpleSelenium.common.driver;

public class UnsupportedOperationgSystemException extends RuntimeException {

	private static final long serialVersionUID = -7688765359234270920L;

	public UnsupportedOperationgSystemException() {
	}

	public UnsupportedOperationgSystemException(final String message) {
		super(message);
	}

	public UnsupportedOperationgSystemException(final Throwable cause) {
		super(cause);
	}

	public UnsupportedOperationgSystemException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public UnsupportedOperationgSystemException(final String message, final Throwable cause, final boolean enableSuppression,
			final boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
