package com.mezawadzki.simpleSelenium.utils;

import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *
 * @author http://www.swtestacademy.com/selenium-wait-javascript-angular-ajax/
 *
 */
public class JSWaiter {
	/**
	 * Logger for this class
	 */
	private static final Logger LOGGER = LogManager.getLogger(JSWaiter.class.getName());

	/** pauses to allow js libs to catch up) */
	public static final int SLEEP_SECONDS = 5;
	private static WebDriver jsWaitDriver;
	private static WebDriverWait jsWait;
	private static JavascriptExecutor jsExec;

	/** Get the driver */
	public static void setDriver(WebDriver driver) {
		jsWaitDriver = driver;
		jsWait = new WebDriverWait(jsWaitDriver, 10);
		jsExec = (JavascriptExecutor) jsWaitDriver;
	}

	/** Wait for JQuery Load */
	public static void waitForJQueryLoad() {
		// Wait for jQuery to load
		ExpectedCondition<Boolean> jQueryLoad = driver -> ((Long) ((JavascriptExecutor) jsWaitDriver)
				.executeScript("return jQuery.active") == 0);

		// Get JQuery is Ready
		boolean jqueryReady = (Boolean) jsExec.executeScript("return jQuery.active==0");

		// Wait JQuery until it is Ready!
		if (!jqueryReady) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("waitForJQueryLoad() - {}", "JQuery is NOT Ready!"); //$NON-NLS-1$ //$NON-NLS-2$
			}
			// Wait for jQuery to load
			jsWait.until(jQueryLoad);
		} else {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("waitForJQueryLoad() - {}", "JQuery is Ready!"); //$NON-NLS-1$ //$NON-NLS-2$
			}
		}
	}

	/** Wait for Angular Load */
	public static void waitForAngularLoad() {
		WebDriverWait wait = new WebDriverWait(jsWaitDriver, 15);
		JavascriptExecutor jsExec = (JavascriptExecutor) jsWaitDriver;

		final String angularReadyScript = "return angular.element(document).injector().get('$http').pendingRequests.length === 0";

		// Wait for ANGULAR to load
		ExpectedCondition<Boolean> angularLoad = driver -> Boolean
				.valueOf(((JavascriptExecutor) driver).executeScript(angularReadyScript).toString());

		// Get Angular is Ready
		boolean angularReady = Boolean.valueOf(jsExec.executeScript(angularReadyScript).toString());

		// Wait ANGULAR until it is Ready!
		if (!angularReady) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("waitForAngularLoad() - {}", "ANGULAR is NOT Ready!"); //$NON-NLS-1$ //$NON-NLS-2$
			}
			// Wait for Angular to load
			wait.until(angularLoad);
		} else {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("waitForAngularLoad() - {}", "ANGULAR is Ready!"); //$NON-NLS-1$ //$NON-NLS-2$
			}
		}
	}

	/**
	 * Wait Until JS Ready
	 *
	 * @return
	 */
	public static Boolean waitUntilJSReady() {
		WebDriverWait wait = new WebDriverWait(jsWaitDriver, 15);
		JavascriptExecutor jsExec = (JavascriptExecutor) jsWaitDriver;

		// Wait for Javascript to load
		ExpectedCondition<Boolean> jsLoad = driver -> ((JavascriptExecutor) jsWaitDriver)
				.executeScript("return document.readyState").toString().equals("complete");

		// Get JS is Ready
		boolean jsReady = (Boolean) jsExec.executeScript("return document.readyState").toString().equals("complete");

		// Wait Javascript until it is Ready!
		if (!jsReady) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("waitUntilJSReady() - {}", "JS in NOT Ready!"); //$NON-NLS-1$ //$NON-NLS-2$
			}
			// Wait for Javascript to load
			wait.until(jsLoad);
		} else {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("waitUntilJSReady() - {}", "JS is Ready!"); //$NON-NLS-1$ //$NON-NLS-2$
			}
		}
		return jsReady;
	}

	/** Wait Until JQuery and JS Ready */
	public static void waitUntilJQueryReady() {
		JavascriptExecutor jsExec = (JavascriptExecutor) jsWaitDriver;

		// First check that JQuery is defined on the page. If it is, then wait
		// AJAX
		Boolean jQueryDefined = (Boolean) jsExec.executeScript("return typeof jQuery != 'undefined'");
		if (jQueryDefined == true) {
			// Pre Wait for stability (Optional)
			sleep(SLEEP_SECONDS);

			// Wait JQuery Load
			waitForJQueryLoad();

			// Wait JS Load
			waitUntilJSReady();

			// Post Wait for stability (Optional)
			sleep(SLEEP_SECONDS);
		} else {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("waitUntilJQueryReady() - {}", "jQuery is not defined on this site!"); //$NON-NLS-1$ //$NON-NLS-2$
			}
		}
	}

	/** Wait Until Angular and JS Ready */
	public static void waitUntilAngularReady() {
		JavascriptExecutor jsExec = (JavascriptExecutor) jsWaitDriver;

		// First check that ANGULAR is defined on the page. If it is, then wait
		// ANGULAR
		Boolean angularUnDefined = (Boolean) jsExec.executeScript("return window.angular === undefined");
		if (!angularUnDefined) {
			Boolean angularInjectorUnDefined = (Boolean) jsExec
					.executeScript("return angular.element(document).injector() === undefined");
			if (!angularInjectorUnDefined) {
				// Pre Wait for stability (Optional)
				sleep(SLEEP_SECONDS);

				// Wait Angular Load
				waitForAngularLoad();

				// Wait JS Load
				waitUntilJSReady();

				// Post Wait for stability (Optional)
				sleep(SLEEP_SECONDS);
			} else {
				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug("waitUntilAngularReady() - {}", "Angular injector is not defined on this site!"); //$NON-NLS-1$ //$NON-NLS-2$
				}
			}
		} else {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("waitUntilAngularReady() - {}", "Angular is not defined on this site!"); //$NON-NLS-1$ //$NON-NLS-2$
			}
		}
	}

	/** Wait Until JQuery Angular and JS is ready */
	public static void waitJQueryAngular() {
		waitUntilJQueryReady();
		waitUntilAngularReady();
	}

	/**
	 * Sleep for 'seconds'.
	 *
	 * @param seconds
	 *            just what it says.
	 */
	public static void sleep(Integer seconds) {
		long secondsLong = (long) seconds;
		try {
			Thread.sleep(TimeUnit.SECONDS.toMillis(secondsLong));
		} catch (InterruptedException e) {
			LOGGER.error("sleep(Integer)", e); //$NON-NLS-1$
		}
	}

	public static void waitDojoReady() {
		//TODO
	}
}