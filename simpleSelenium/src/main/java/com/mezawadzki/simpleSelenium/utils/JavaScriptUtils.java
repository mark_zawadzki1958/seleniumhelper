package com.mezawadzki.simpleSelenium.utils;

import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

public class JavaScriptUtils {
	/**
	 * Logger for this class
	 */
	private static final Logger LOGGER = LogManager.getLogger(JavaScriptUtils.class.getName());
	private static String XHR_TEMPLATE = "var callback = arguments[arguments.length - 1];"
			+ " jQuery.getJSON('%s', function (data) { callback(JSON.stringify(data)); });";

	private static String XHR_TEMPLATE2 = "xhr = new XMLHttpRequest();"
			+ "var callback = arguments[arguments.length - 1];" + "$(document).ready(function() {" + "  $.ajax({"
			+ "   url: '%s,'" + "   type: 'GET'," + "   data: function (data) { callback(JSON.stringify(data)); }),"
			+ "   datatype: 'json'," + "   success: function() { }," + "   error: function() {  },"
			+ "   beforeSend: setHeader   " + "  }); " + "});" + "function setHeader(xhr) {"
			+ "  xhr.setRequestHeader('range', 'items=0-9');" + "}";

	/**
	 * Inject script onto page.
	 *
	 * @param driver
	 *            driver
	 * @param scriptURL
	 *            URL of script
	 * @throws Exception
	 *             {@link java.lang.Exception}
	 */
	public void injectScript(final WebDriver driver, final String scriptURL) throws Exception {
		final JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("function injectScript(url) {\n" + "    var script = document.createElement('script');\n"
				+ "    script.src = url;\n" + "    var head = document.getElementsByTagName('head')[0];\n"
				+ "    head.appendChild(script);\n" + "}\n\n" + "var scriptURL = arguments[0];\n"
				+ "injectScript(scriptURL);", scriptURL);
	}

	/**
	 * HDoes the page have JQuery loaded.
	 *
	 * @param driver
	 * @return true if loaded, else false
	 * @throws Exception
	 *             {@java.lang.Exception}
	 */
	public Boolean isjQueryLoaded(final WebDriver driver) throws Exception {
		final JavascriptExecutor js = (JavascriptExecutor) driver;
		return (Boolean) js.executeScript("return typeof jQuery != 'undefined';");
	}

	/**
	 * Given a URL (REST GET) request get the response JSON string. </b> Default
	 * timeout of 5 seconds.
	 *
	 * @param driver
	 * @param url
	 *            URL of request.
	 * @return response JSON
	 * @throws Exception
	 *             {@link java.lang.Exception}
	 * @see com.mezawadzki.simpleSelenium.common.rest.JavaScriptUtils.getJSON(WebDriver,
	 *      String, long, TimeUnit)
	 */
	public String getJSON(final WebDriver driver, final String url) throws Exception {
		return getJSON(driver, url, 5, TimeUnit.SECONDS);
	}

	/**
	 * Given a URL (REST GET) request get the response JSON string. </b> Default
	 * timeout of 5 seconds.
	 *
	 * @param driver
	 * @param url
	 *            URL of request.
	 * @param timeout
	 *            The timeout value
	 * @param timeUnit
	 *            Unit of time.
	 * @return response JSON
	 * @throws Exception
	 *             {@link java.lang.Exception}
	 * @see com.mezawadzki.simpleSelenium.common.rest.JavaScriptUtils.getJSON(WebDriver,
	 *      String, long, TimeUnit)
	 */
	public String getJSON(final WebDriver driver, final String url, final long timeout, final TimeUnit timeUnit) throws Exception {
		final JavascriptExecutor js = (JavascriptExecutor) driver;

		// Set ScriptTimeout
		driver.manage().timeouts().setScriptTimeout(timeout, timeUnit);
		final String script = String.format(XHR_TEMPLATE, url);
		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("getJSON(WebDriver, String) - String script={}", script); //$NON-NLS-1$
		}

		final Object response = js.executeAsyncScript(script);

		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("getJSON(WebDriver, String) - Object response={}", response); //$NON-NLS-1$
		}

		return (String) response;
	}

	public String getJSonWithRange(final WebDriver driver, final String url, final long timeout, final TimeUnit timeUnit) throws Exception {
		final JavascriptExecutor js = (JavascriptExecutor) driver;

		// Set ScriptTimeout
		driver.manage().timeouts().setScriptTimeout(timeout, timeUnit);
		final String script = String.format(XHR_TEMPLATE2, url);
		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("getJSON(WebDriver, String) - String script={}", script); //$NON-NLS-1$
		}

		final Object response = js.executeAsyncScript(script);

		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("getJSON(WebDriver, String) - Object response={}", response); //$NON-NLS-1$
		}

		return (String) response;

	}
}