/**
 *
 */
package com.mezawadzki.simpleSelenium;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Mark
 *
 */
public class DefaultLifeCycle implements LifeCycle {
	/**
	 * Logger for this class
	 */
	private static final Logger LOGGER = LogManager.getLogger(DefaultLifeCycle.class.getName());

	private final String id;

	private TestData data;
	/**
	 * Default construcor ... GUID used as id.
	 */
	public DefaultLifeCycle() {
		this.id = java.util.UUID.randomUUID().toString();
	}

	/**
	 * Use custom id.
	 * @param id identifier to be used in lifecycle..
	 */
	public DefaultLifeCycle(final String id) {
		this.id = id;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.mezawadzki.simpleSelenium.LifeCycle#before()
	 */
	@Override
	public TestData before() {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("DefaultLifeCycle.before() - id={}", id); //$NON-NLS-1$
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.mezawadzki.simpleSelenium.LifeCycle#after()
	 */
	@Override
	public void after(final Map<String, String> map) {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("DefaultLifeCycle.after() - id={}", id); //$NON-NLS-1$
			LOGGER.debug("DefaultLifeCycle.after() - map={}", map); //$NON-NLS-1$
		}

	}

	/**
	 * @return the data
	 */
	public TestData getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(final TestData data) {
		this.data = data;
	}

}
