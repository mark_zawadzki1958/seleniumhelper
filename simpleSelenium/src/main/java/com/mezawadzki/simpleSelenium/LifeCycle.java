/**
 *
 */
package com.mezawadzki.simpleSelenium;

import java.util.Map;

/**
 * before and after test actions.
 * @author Mark
 *
 */
public interface LifeCycle {


	/**
	 * Do things before the test.
	 */
	TestData before();


	/**
	 * run this after test
	 * @param map - stuff to use in the after method
	 */
	void after(Map<String, String> map);
}
