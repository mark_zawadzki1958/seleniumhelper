package com.mezawadzki.simpleSelenium;

import static org.hamcrest.Matchers.equalTo;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Rule;
import org.junit.rules.ErrorCollector;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.Point;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.mezawadzki.simpleSelenium.common.Configuration;
import com.mezawadzki.simpleSelenium.common.GeneralUtils;
import com.mezawadzki.simpleSelenium.common.MaxRetryException;
import com.mezawadzki.simpleSelenium.common.OnFailureAction;
import com.mezawadzki.simpleSelenium.common.transform.Transformer;
import com.mezawadzki.simpleSelenium.utils.JSWaiter;
import com.mezawadzki.simpleSelenium.utils.JavaScriptUtils;

//TODO catch exceptions so that they don't just fail out. Report them somehow.
public abstract class WebTestBase {
	/** potential browser values */
	protected static enum Browsers {
		CHROME, FIREFOX, IE, OPERA, UNKNOWN
	}

	private static Browsers browserName = Browsers.UNKNOWN;

	protected static WebDriver driver = null;

	/**
	 * Logger for this class
	 */
	private static final Logger LOGGER = LogManager.getLogger(WebTestBase.class.getName());

	public static final int WAIT_FOR_LOAD_DURATION_SECONDS = 3;

	/** JavaScript injection utilities */
	public static final JavaScriptUtils jsonUtils = new JavaScriptUtils();

	/**
	 * Begin testing, logging into page.
	 *
	 * @throws Exception
	 */
	protected static void beginTesting() throws Exception {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("beginTesting() - Testing using browser = ", System.getProperty("browser")); //$NON-NLS-1$
		}
		driver = com.mezawadzki.simpleSelenium.common.AccessApplication.login();
	}

	public static WebDriver getDriver() {
		return driver;
	}

	public static void setDriver(final WebDriver driver) {
		WebTestBase.driver = driver;
	}

	/**
	 * Sleep for 'seconds'.
	 *
	 * @param seconds
	 *            just what it says.
	 */
	protected static void sleep(final Integer seconds) {
		final long secondsLong = seconds;
		try {
			Thread.sleep(TimeUnit.SECONDS.toMillis(secondsLong));
		} catch (final InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Rule
	public ErrorCollector collector = new ErrorCollector();

	protected SimpleDateFormat formatter = new SimpleDateFormat("MMMMM dd, yyyy h:mm a");

	protected SimpleDateFormat formatterWithZone = new SimpleDateFormat("MM/dd/yyyy hh:mm a z");

	@Rule
	public OnFailureAction onFailedRule = new OnFailureAction();

	/**
	 * force click on target by using a more direct process
	 *
	 * @param target
	 */
	public void forceClick(final WebElement target) {
		new Actions(driver).moveToElement(target).click();
	}

	/**
	 * Return all attributes in a map.
	 *
	 * @param driver
	 *            driver
	 * @param webElement
	 *            webElement
	 * @return map of attributes to value
	 */
	protected Map<String, String> allAttributesAsMap(final WebElement webElement) {
		final JavascriptExecutor executor = (JavascriptExecutor) driver;
		@SuppressWarnings("unchecked")
		final Map<String, String> map = (Map<String, String>) executor
		.executeScript("var items = {}; for (index = 0; index < arguments[0].attributes.length; ++index) "
				+ "{ " + "items[arguments[0].attributes[index].name] = arguments[0].attributes[index].value "
				+ "}; " + "return items;", webElement);

		return map;
	}

	/**
	 * Check 'text' and text area inputs and their supporting cast.
	 *
	 * @param formEl
	 *            form containing
	 * @param byForLabel
	 *            "By" to find label.
	 * @param labelValues
	 *            labels expected values (one matches).
	 * @param byForInput
	 *            "By" to find input element.
	 * @param inputValue
	 *            input element's expected value.
	 */
	protected void checkTextInput(final WebElement formEl, final LifeCycle lifeCycle, final By labelXPath,
			final String[] labelValues, final By inputXPath, final String inputValue) {
		checkTextInput(formEl, lifeCycle, labelXPath, labelValues, inputXPath, inputValue, null, null);
	}

	/**
	 * Check 'text' and text area inputs and their supporting cast.
	 *
	 * @param formEl
	 *            form containing
	 * @param lifeCycle
	 * @see {@link com.mezawadzki.simpleSelenium.LifeCycle}
	 * @param byForLabel
	 *            "By" to find label.
	 * @param labelValue
	 *            label's expected value.
	 * @param byForInput
	 *            "By" to find input element.
	 * @param inputValue
	 *            input element's expected value.
	 */
	protected void checkTextInput(final WebElement formEl, final LifeCycle lifeCycle, final By labelXPath,
			final String labelValue, final By inputXPath, final String inputValue) {
		checkTextInput(formEl, lifeCycle, labelXPath, labelValue, inputXPath, inputValue, null, null);
	}

	/**
	 * Check 'text' and text area inputs and their supporting cast.
	 *
	 * @param formEl
	 *            form containing
	 * @param lifeCycle
	 * @see {@link com.mezawadzki.simpleSelenium.LifeCycle}
	 * @param byForLabel
	 *            "By" to find label.
	 * @param labelValue
	 *            label's expected value.
	 * @param byForInput
	 *            "By" to find input element.
	 * @param inputValue
	 *            input element's expected value.
	 * @param byForHiddenInput
	 *            "By" to find "hidden' element which contains additional data.
	 *            Usually the id (primary key) if the input value.
	 * @param hiddenValue
	 *            hidden element's expected value.
	 */
	protected void checkTextInput(final WebElement formEl, final By byForLabel, final String labelValue,
			final By byForInput, final String inputValue, final By byForHiddenInput, final String hiddenValue) {
		final DefaultLifeCycle lifeCycle = new DefaultLifeCycle();
		final TestData data = new TestData();
		data.setData(inputValue);
		lifeCycle.setData(data);
		// TODO use Lifecycle for input value.
		checkTextInput(formEl, lifeCycle, byForLabel, labelValue, byForInput, inputValue, null, byForHiddenInput,
				hiddenValue, null);
	}

	/**
	 * Check 'text' and text area inputs and their supporting cast.
	 *
	 * @param formEl
	 *            form containing
	 * @param lifeCycle
	 * @see {@link com.mezawadzki.simpleSelenium.LifeCycle}
	 * @param byForLabel
	 *            "By" to find label.
	 * @param labelValue
	 *            label's expected value.
	 * @param byForInput
	 *            "By" to find input element.
	 * @param inputValue
	 *            input element's expected value.
	 * @param byForHiddenInput
	 *            "By" to find "hidden' element which contains additional data.
	 *            Usually the id (primary key) if the input value.
	 * @param hiddenValue
	 *            hidden element's expected value.
	 */
	protected void checkTextInput(final WebElement formEl, final LifeCycle lifeCycle, final By byForLabel,
			final String labelValue, final By byForInput, final String inputValue, final By byForHiddenInput,
			final String hiddenValue) {
		checkTextInput(formEl, lifeCycle, byForLabel, labelValue, byForInput, inputValue, null, byForHiddenInput,
				hiddenValue, null);
	}

	/**
	 * Check 'text' and text area inputs and their supporting cast.
	 *
	 * @param formEl
	 *            form containing
	 * @param byForLabel
	 *            "By" to find label.
	 * @param labelValue
	 *            label's expected values.
	 * @param byForInput
	 *            "By" to find input element.
	 * @param inputValue
	 *            input element's expected value.
	 * @param byForHiddenInput
	 *            "By" to find "hidden' element which contains additional data.
	 *            Usually the id (primary key) if the input value.
	 * @param hiddenValue
	 *            hidden element's expected value.
	 */
	protected void checkTextInput(final WebElement formEl, final LifeCycle lifeCycle, final By byForLabel,
			final String labelValue[], final By byForInput, final String inputValue, final By byForHiddenInput,
			final String hiddenValue) {
		checkTextInput(formEl, lifeCycle, byForLabel, labelValue, byForInput, inputValue, null, byForHiddenInput,
				hiddenValue, null);
	}

	/**
	 * Check 'text' and text area inputs and their supporting cast.
	 *
	 * @param formEl
	 *            form containing
	 * @param lifeCycle
	 * @see {@link com.mezawadzki.simpleSelenium.LifeCycle}
	 * @param byForLabel
	 *            "By" to find label.
	 * @param labelValue
	 *            label's expected value.
	 * @param byForInput
	 *            "By" to find input element.
	 * @param inputValue
	 *            input element's expected value.
	 * @param transformInput
	 *            class to be used to transform the inputValue, may be null.
	 */
	protected void checkTextInput(final WebElement formEl, final LifeCycle lifeCycle, final By byForLabel,
			final String labelValue, final By byForInput, final String inputValue, final Transformer transformInput) {
		checkTextInput(formEl, lifeCycle, byForLabel, labelValue, byForInput, inputValue, transformInput, null, null,
				null);
	}

	/**
	 * Check 'text' and text area inputs and their supporting cast.
	 *
	 * @param formEl
	 *            form containing
	 * @param byForLabel
	 *            "By" to find label.
	 * @param labelValue
	 *            label's expected value.
	 * @param byForInput
	 *            "By" to find input element.
	 * @param inputValue
	 *            input element's expected value.
	 * @param transformInput
	 *            class to be used to transform the inputValue, may be null.
	 */
	protected void checkTextInput(final WebElement formEl, final LifeCycle lifeCycle, final By byForLabel,
			final String[] labelValue, final By byForInput, final String inputValue, final Transformer transformInput) {
		checkTextInput(formEl, lifeCycle, byForLabel, labelValue, byForInput, inputValue, transformInput, null, null,
				null);
	}

	/**
	 * Check 'text' and text area inputs and their supporting cast.
	 *
	 * @param formEl
	 *            form containing
	 * @param lifeCycle
	 * @see {@link com.mezawadzki.simpleSelenium.LifeCycle}
	 * @param byForLabel
	 *            "By" to find label.
	 * @param labelValue
	 *            label's expected value.
	 * @param byForInput
	 *            "By" to find input element.
	 * @param inputValue
	 *            input element's expected value.
	 * @param transformInput
	 *            class to be used to transform the inputValue, may be null.
	 * @param byForHiddenInput
	 *            "By" to find "hidden' element which contains additional data.
	 *            Usually the id (primary key) if the input value.
	 * @param hiddenValue
	 *            hidden element's expected value.
	 *
	 */
	protected void checkTextInput(final WebElement formEl, final LifeCycle lifeCycle, final By byForLabel,
			final String labelValue, final By byForInput, final String inputValue, final Transformer transformInput,
			final By byForHiddenInput, final String hiddenValue) {
		checkTextInput(formEl, lifeCycle, byForLabel, labelValue, byForInput, inputValue, transformInput,
				byForHiddenInput, hiddenValue, null);
	}

	/**
	 * Check 'text' and text area inputs and their supporting cast.
	 *
	 * @param formEl
	 *            form containing
	 * @param lifeCycle
	 * @see {@link com.mezawadzki.simpleSelenium.LifeCycle}
	 * @param byForLabel
	 *            "By" to find label.
	 * @param labelValue
	 *            label's expected value.
	 * @param byForInput
	 *            "By" to find input element.
	 * @param inputValue
	 *            input element's expected value.
	 * @param transformInput
	 *            class to be used to transform the inputValue, may be null.
	 *            Transforms both page value and expected.
	 * @param byForHiddenInput
	 *            "By" to find "hidden' element which contains additional data.
	 *            Usually the id (primary key) if the input value.
	 * @param hiddenValue
	 *            hidden element's expected value.
	 * @param transformHidden
	 *            class to be used to transform the hiddenValue text, may be null.
	 *            Transforms both page value and expected.
	 */
	protected void checkTextInput(final WebElement formEl, final LifeCycle lifeCycle, final By byForLabel,
			final String labelValue, final By byForInput, String inputValue, final Transformer transformInput,
			final By byForHiddenInput, String hiddenValue, final Transformer transformHidden) {

		final TestData testData = lifeCycle.before();
		// TODO catch exceptions so that they don't just fail out. Report them
		// somehow.
		WebElement label;
		WebElement element;
		WebElement hidden;
		label = findWithWait(formEl, byForLabel);
		collector.checkThat(label.getText(), equalTo(labelValue));
		element = findWithWait(formEl, byForInput);

		String elValue = element.getAttribute("value");
		if (transformInput != null) {
			elValue = transformInput.normalize(elValue);
			inputValue = transformInput.normalize(inputValue);
		}
		collector.checkThat("Input value mismatch", elValue, equalTo(inputValue));

		if (byForHiddenInput != null) {
			hidden = formEl.findElement(byForHiddenInput);
			elValue = hidden.getAttribute("value");
			if (transformHidden != null) {
				elValue = transformHidden.normalize(elValue);
				hiddenValue = transformHidden.normalize(hiddenValue);
			}
			collector.checkThat("'Hidden' value mismatch", elValue, equalTo(hiddenValue));
		}
		// TODO the map should be malleble.
		lifeCycle.after(new HashMap<String, String>());
	}

	/**
	 * Check 'text' and text area inputs and their supporting cast.
	 *
	 * @param formEl
	 *            form containing
	 * @param byForLabel
	 *            "By" to find label.
	 * @param labelValue
	 *            label's expected value.
	 * @param byForInput
	 *            "By" to find input element.
	 * @param inputValue
	 *            input element's expected value.
	 * @param transformInput
	 *            class to be used to transform the inputValue, may be null.
	 *            Transforms both page value and expected.
	 * @param byForHiddenInput
	 *            "By" to find "hidden' element which contains additional data.
	 *            Usually the id (primary key) if the input value.
	 * @param hiddenValue
	 *            hidden element's expected value.
	 * @param transformHidden
	 *            class to be used to transform the hiddenValue text, may be null.
	 *            Transforms both page value and expected.
	 */
	protected void checkTextInput(final WebElement formEl, final LifeCycle lifeCycle, final By byForLabel,
			final String labelValue[], final By byForInput, String inputValue, final Transformer transformInput,
			final By byForHiddenInput, String hiddenValue, final Transformer transformHidden) {

		// TODO catch exceptions so that they don't just fail out. Report them
		// somehow.
		WebElement label;
		WebElement element;
		WebElement hidden;
		label = findWithWait(formEl, byForLabel);

		boolean match = false;
		for (final String val : labelValue) {
			if (val.equals(label.getText())) {
				match = true;
				break;
			}
		}
		collector.checkThat(String.format("No match found for %s in %s.", label.getText(), Arrays.toString(labelValue)),
				match, equalTo(true));
		element = findWithWait(formEl, byForInput);

		String elValue = element.getAttribute("value");
		if (transformInput != null) {
			elValue = transformInput.normalize(elValue);
			inputValue = transformInput.normalize(inputValue);
		}
		collector.checkThat("Input value mismatch", elValue, equalTo(inputValue));

		if (byForHiddenInput != null) {
			hidden = formEl.findElement(byForHiddenInput);
			elValue = hidden.getAttribute("value");
			if (transformHidden != null) {
				elValue = transformHidden.normalize(elValue);
				hiddenValue = transformHidden.normalize(hiddenValue);
			}
			collector.checkThat("'Hidden' value mismatch", elValue, equalTo(hiddenValue));
		}

	}

	protected WebElement clearAndSendKeys(final WebElement webElement, final CharSequence keysToSend) {
		webElement.clear();
		webElement.sendKeys(keysToSend);
		return webElement;
	}

	/**
	 * Click button expect to see the result element.
	 *
	 * @param toClick
	 *            element to click.
	 * @param expectToSee
	 *            element expected to be presented as a result offclick.
	 */
	protected void clickBttn(final By toClick, final By expectToSee) {
		directClick(toClick);
		waitForLoad(10);
		findWithWait(expectToSee, 10);

	}

	/** click on each link in page. */
	public void clickLinks() throws Exception {
		try {
			final List<WebElement> componentList = driver.findElements(By.tagName("a"));
			System.out.println(componentList.size());

			for (final WebElement component : componentList) {
				// click1();
				System.out.println(component.getAttribute("href"));
			}
			final int numberOfElementsFound = getNumberOfElementsFound(By.tagName("a"));
			for (int pos = 0; pos < numberOfElementsFound; pos++) {
				if (getElementWithIndex(By.tagName("a"), pos).isDisplayed()) {

					getElementWithIndex(By.tagName("a"), pos).click();
					Thread.sleep(200);
					driver.navigate().back();
					Thread.sleep(200);
				}
			}
		} catch (final Exception e) {
			System.out.println("error in getLinks " + e);
		}
	}

	/**
	 * Use JavascriptExecutor to directly click on the DOM element.
	 *
	 * @param element
	 *            to click.
	 */
	protected void directClick(final By by) {
		final JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", driver.findElement(by));
	}

	/**
	 * Use JavascriptExecutor to directly click on the DOM element.
	 *
	 * @param element
	 *            to click.
	 */
	protected void directClick(final WebElement element) {
		final JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", element);
	}

	/**
	 * rows that exist beneath the element identified by <i>by</i>.
	 *
	 * @param by
	 *            By object of containing element. Be as specific as possible. Table
	 *            or Tbody are ideal.
	 * @param timeout
	 *            timeout
	 */
	protected List<WebElement> findCellsIn(final By by, final long timeout) {
		waitForLoad(5);
		final WebElement webElement = findWithWait(by);
		return webElement.findElements(By.xpath("//td"));
	}

	/**
	 * rows exist beneath the tBodyId el. No explicit wit. be careful
	 *
	 * @param webElement
	 *            Containing webElement. Be as specific as possible.
	 * @param timeout
	 *            timeout
	 */
	protected List<WebElement> findCellsIn(final WebElement webElement, final long timeout) {
		return webElement.findElements(By.xpath("//td"));
	}

	/**
	 * Draws a red border around the found element. Does not set it back anyhow.
	 *
	 * @param by
	 *            locator
	 * @return element selected..
	 */
	public WebElement findElementHightlight(final By by, final String colour) {
		WebElement elem = null;
		try {
			elem = driver.findElement(by);
			// draw a border around the found element
			highLight(elem, colour);
		} catch (final Exception e) {
			collector.addError(new Throwable(e.getMessage()));
		}
		return elem;
	}

	/**
	 * Find elements by.
	 *
	 * @param by
	 * @return
	 */
	protected List<WebElement> findElements(final By by) {
		// TODO catch exceptions so that they don't just fail out. Report them
		// somehow.
		return findElements(by, Configuration.DEFAULT_TIMEOUT);
	}

	protected List<WebElement> findElements(final WebElement we, final By by) {
		return we.findElements(by);
	}

	protected List<WebElement> findElements(final By by, final long timeout) {
		// TODO catch exceptions so that they don't just fail out. Report them
		// somehow.
		return new WebDriverWait(driver, timeout).until((final WebDriver dr1) -> dr1.findElements(by));
	}

	/**
	 * rows exist beneath the el. No explicit wait. be careful
	 *
	 * @param webElement
	 *            Containing webElement. Be as specific as possible.
	 * @param timeout
	 *            timeout
	 */
	protected List<WebElement> findElements(final String xpathExpression) {
		// TODO catch exceptions so that they don't just fail out. Report them
		// somehow.
		return driver.findElements(By.xpath(xpathExpression));

	}

	/**
	 * rows exist beneath the el. No explicit wait. be careful
	 *
	 * @param webElement
	 *            Containing webElement. Be as specific as possible.
	 * @param timeout
	 *            timeout
	 */
	protected List<WebElement> findElements(final WebElement parent, final String xpathExpression) {
		// TODO catch exceptions so that they don't just fail out. Report them
		// somehow.
		return parent.findElements(By.xpath(xpathExpression));
	}

	/**
	 * Find stale element with retry. Recursive.
	 *
	 * @param by
	 *            web element to find * @throws <code>RuntimeException</code>
	 *            {@link com.mezawadzki.simpleSelenium.common.MaxRetryException}
	 *            tried greater than maxTries.
	 */
	protected WebElement findElusiveElement(final By by) {
		return findElusiveElement(by, Configuration.DEFAULT_TIMEOUT, Configuration.DEFAULT_POLLING_EVERY, 0);
	}

	/**
	 * Find stale element with retry. Recursive.
	 *
	 * @param by
	 *            web element to find
	 * @param timeOut
	 *            timeout in seconds for each retry.
	 * @param maxTries
	 *            attempt at most this number of times
	 * @throws <code>RuntimeException</code>
	 *             {@link com.mezawadzki.simpleSelenium.common.MaxRetryException}
	 *             tried greater than maxTries.
	 */
	protected WebElement findElusiveElement(final By by, final long timeOut, final long pollingEvery,
			final int maxTries) {
		return findElusiveElement(by, timeOut, pollingEvery, maxTries, 0);
	}

	/**
	 * Find stale element with retry. Recursive.
	 *
	 * @param by
	 *            web element to find
	 * @param timeOut
	 *            timeout in seconds for each retry.
	 * @param maxTries
	 *            attempt at most this number of times
	 * @param tryCount
	 *            current attempt number
	 * @return weblElement identified by <code>by</code.
	 * @throws <code>RuntimeException</code>
	 *             {@link com.mezawadzki.simpleSelenium.common.MaxRetryException}
	 *             tried greater than maxTries.
	 */
	private WebElement findElusiveElement(final By by, final long timeOut, final long pollingEvery, final int maxTries,
			int tryCount) {
		WebElement webElement = null;
		try {
			webElement = findWithWait(by, timeOut, pollingEvery);
		} catch (StaleElementReferenceException | NoSuchElementException e) {
			System.out.println("Attempting to recover from " + e.getClass().getSimpleName() + "...");
			if (tryCount <= maxTries) {
				webElement = findElusiveElement(by, timeOut, pollingEvery, tryCount++);
			}
			throw new MaxRetryException("Retried " + maxTries + " and failed.", e);
		}
		return webElement;
	}

	/**
	 * Find stale element with retry. Recursive.
	 *
	 * @param container
	 *            WebElement containing the target.
	 * @param by
	 *            web element to find
	 * @param timeOut
	 *            timeout in seconds for each retry.
	 * @param maxTries
	 *            attempt at most this number of times
	 * @param tryCount
	 *            current attempt number
	 * @return weblElement identified by <code>by</code.
	 * @throws <code>RuntimeException</code>
	 *             {@link com.mezawadzki.simpleSelenium.common.MaxRetryException}
	 *             tried greater than maxTries.
	 */

	private WebElement findElusiveElement(final WebElement container, final By by, final long timeOut,
			final long pollingEvery, final int maxTries, int tryCount) {
		WebElement webElement = null;
		try {
			webElement = findWithWait(container, by, timeOut, pollingEvery);
		} catch (StaleElementReferenceException | NoSuchElementException e) {
			System.out.println("Attempting to recover from " + e.getClass().getSimpleName() + "...");
			if (tryCount <= maxTries) {
				webElement = findElusiveElement(by, timeOut, pollingEvery, tryCount++);
			}
			throw new MaxRetryException("Retried " + maxTries + " and failed.", e);
		}
		return webElement;
	}

	/**
	 * Find stale element with retry. Recursive.
	 *
	 * @param container
	 *            WebElement containing the target.
	 * @param by
	 *            web element to find
	 * @param timeOut
	 *            timeout in seconds for each retry.
	 * @param maxTries
	 *            attempt at most this number of times
	 * @return weblElement identified by <code>by</code.
	 * @throws <code>RuntimeException</code>
	 *             {@link com.mezawadzki.simpleSelenium.common.MaxRetryException}
	 *             tried greater than maxTries.
	 */
	public WebElement findElusiveElement(final WebElement container, final By by, final long timeOut,
			final long pollingEvery, final int maxTries) {
		return findElusiveElement(container, by, timeOut, pollingEvery, maxTries, 0);
	}

	/**
	 * Find stale element with retry. Recursive.
	 *
	 * @param container
	 *            WebElement containing the target.
	 * @param by
	 *            web element to find
	 * @return weblElement identified by <code>by</code.
	 * @throws <code>RuntimeException</code>
	 *             {@link com.mezawadzki.simpleSelenium.common.MaxRetryException}
	 *             tried greater than maxTries.
	 */
	public WebElement findElusiveElement(final WebElement container, final By by) {
		return findElusiveElement(container, by, Configuration.SHORT_TIMEOUT, Configuration.SHORT_POLLING_EVERY,
				Configuration.SHORT_MAX_RETRIES);
	}

	/**
	 * Find stale element with retry. Recursive.
	 *
	 * @param containerBy
	 *            <code>By</code> containing the target.
	 * @param by
	 *            web element to find
	 * @param timeOut
	 *            timeout in seconds for each retry.
	 * @param maxTries
	 *            attempt at most this number of times
	 * @return weblElement identified by <code>by</code.
	 * @throws <code>RuntimeException</code>
	 *             {@link com.mezawadzki.simpleSelenium.common.MaxRetryException}
	 *             tried greater than maxTries.
	 */
	public WebElement findElusiveElement(final By containerBy, final By by, final long timeOut, final long pollingEvery,
			final int maxTries) {
		return findElusiveElement(driver.findElement(containerBy), by, timeOut, pollingEvery, maxTries, 0);
	}

	/**
	 * Find stale element with retry. Recursive.
	 *
	 * @param containerBy
	 *            <code>By</code> containing the target.
	 * @param by
	 *            web element to find
	 * @param timeOut
	 *            timeout in seconds for each retry.
	 * @param maxTries
	 *            attempt at most this number of times
	 * @return weblElement identified by <code>by</code.
	 * @throws <code>RuntimeException</code>
	 *             {@link com.mezawadzki.simpleSelenium.common.MaxRetryException}
	 *             tried greater than maxTries.
	 */
	public WebElement findElusiveElement(final By containerBy, final By by) {
		return findElusiveElement(driver.findElement(containerBy), by, Configuration.SHORT_TIMEOUT,
				Configuration.SHORT_POLLING_EVERY, Configuration.SHORT_MAX_RETRIES, 0);
	}

	/**
	 * rows that exist beneath the element identified by <i>by</i>.
	 *
	 * @param by
	 *            By object of containing element. Be as specific as possible. Table
	 *            or Tbody are ideal.
	 * @param timeout
	 *            timeout
	 */
	protected List<WebElement> findRowsIn(final By by, final long timeout) {
		// TODO catch exceptions so that they don't just fail out. Report them
		// somehow.
		waitForLoad(5);
		final WebElement webElement = findWithWait(by);
		return webElement.findElements(By.xpath("//tr"));
	}

	/**
	 * rows exist beneath the el. No explicit wit. be careful
	 *
	 * @param webElement
	 *            Containing webElement. Be as specific as possible.
	 * @param timeout
	 *            timeout
	 */
	protected List<WebElement> findRowsIn(final WebElement webElement, final long timeout) {
		// TODO catch exceptions so that they don't just fail out. Report them
		// somehow.
		return webElement.findElements(By.xpath("//tr"));
	}

	/**
	 * Find element by (By by) with default timeout wait.
	 *
	 * @param by
	 * @return WebElement
	 */
	protected WebElement findWithWait(final By by) {
		return findWithWait(by, Configuration.DEFAULT_TIMEOUT);
	}

	/**
	 * Find element by (By by) with timeout wait.
	 *
	 * @param by
	 * @param timeout
	 * @return WebElement
	 */
	protected WebElement findWithWait(final By by, final long timeout) {

		return findWithWait(driver, by, timeout, Configuration.DEFAULT_POLLING_EVERY);
	}

	/**
	 * Find element by (By by) with timeout wait.
	 *
	 * @param by
	 * @param timeout
	 * @return WebElement
	 */
	protected WebElement findWithWait(final By by, final long timeout, final long pollingEvery) {
		WebElement webElement;
		try {
			webElement = findWithWait(driver, by, timeout, pollingEvery);
		} catch (final Exception e) {
			LOGGER.error(e);
			throw e;
		}
		return webElement;
	}

	/**
	 * Find element by xpath with default timeout wait.
	 *
	 * @param xpathExpression
	 * @return WebElement
	 */
	protected WebElement findWithWait(final String xpathExpression) {
		return findWithWait(xpathExpression, Configuration.DEFAULT_TIMEOUT);

	}

	/**
	 * Find element by xpath with default timeout wait.
	 *
	 * @param xpathExpression
	 *
	 * @return WebElement
	 */
	protected WebElement findWithWait(final String xpathExpression, final long timeout) {
		return findWithWait(By.xpath(xpathExpression), timeout);

	}

	/**
	 * Find a element with default time out.
	 *
	 * @param driver
	 *            WebDriver instance.
	 * @param By
	 *            By of expected element.
	 * @param timeout
	 *            timeout after this many seconds
	 * @return the web element if exists.
	 */
	protected WebElement findWithWait(final WebDriver driver, final By by, final long timeout,
			final long pollingEvery) {
		// TODO catch exceptions so that they don't just fail out. Report them
		// somehow.
		final Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)

				.withTimeout(timeout, TimeUnit.SECONDS)

				.pollingEvery(2, TimeUnit.SECONDS)

				.ignoring(NoSuchElementException.class);

		final WebElement resultEl = wait.until(driver1 -> {
			WebElement el = null;

			try {
				el = driver1.findElement(by);
			} catch (final org.openqa.selenium.WebDriverException e) {
				LOGGER.error(e.getMessage());
				throw e;
			}

			return el;
		});
		return resultEl;

	}

	/**
	 * Find element identified by "By" within given webelement.
	 *
	 * @param webElement
	 *            container webelement.
	 * @param by
	 *            selenium "By"
	 * @return webelement determined by "By"
	 */
	protected WebElement findWithWait(final WebElement webElement, final By by) {
		return findWithWait(webElement, by, Configuration.DEFAULT_TIMEOUT, Configuration.DEFAULT_POLLING_EVERY);
	}

	/**
	 * Find a element with default time out.
	 *
	 * @param driver
	 *            containing (parent) Web element.
	 * @param By
	 *            By of expected element.
	 * @param timeout
	 *            timeout after this many seconds
	 * @return the web element if exists.
	 */
	protected WebElement findWithWait(final WebElement webElement, final By by, final long timeout,
			final long pollingEvery) {
		// TODO catch exceptions so that they don't just fail out. Report them
		// somehow.
		final Wait<WebElement> wait = new FluentWait<WebElement>(webElement)

				.withTimeout(timeout, TimeUnit.SECONDS)

				.pollingEvery(2, TimeUnit.SECONDS)

				.ignoring(NoSuchElementException.class);

		return wait.until(driver -> driver.findElement(by));
	}

	/**
	 * browser name.
	 *
	 * @return browser identifier.
	 */
	protected Browsers getCurrentBrowserName() {
		// lazy init.
		if (driver instanceof JavascriptExecutor && Browsers.UNKNOWN.equals(browserName)) {
			final JavascriptExecutor js = (JavascriptExecutor) driver;
			if (LOGGER.isInfoEnabled()) {
				LOGGER.info("getCurrentBrowserName() - JavascriptExecutor js={}", js.toString()); //$NON-NLS-1$
			}
			if (driver instanceof ChromeDriver) {
				browserName = Browsers.CHROME;
			} else if (driver instanceof FirefoxDriver) {
				browserName = Browsers.FIREFOX;
			} else if (driver instanceof OperaDriver) {
				browserName = Browsers.OPERA;
			}
		}
		return browserName;
	}

	/**
	 * Harvest texts from a list of web elements.
	 *
	 * @param webElements
	 * @return list of text.
	 */
	protected List<String> getElementsText(final List<WebElement> webElements) {
		final List<String> texts = new ArrayList<>();
		for (final WebElement webElement : webElements) {
			texts.add(webElement.getText());
		}
		return texts;
	}

	/**
	 * Get element located by <code>by</code> and at index <code>pos</code>.
	 *
	 * @param by
	 *            locator
	 * @param pos
	 *            index
	 * @return web element.
	 */
	public WebElement getElementWithIndex(final By by, final int pos) {
		return driver.findElements(by).get(pos);
	}

	/**
	 * Get number of elements found ...
	 *
	 * @param by
	 *            locator
	 * @return number of elements found
	 */
	public int getNumberOfElementsFound(final By by) {
		return driver.findElements(by).size();
	}

	/**
	 * highlight the element determined by 'by' with selected colour.
	 *
	 * @param element
	 *            to highlight. @see org.openqa.selenium.By
	 * @param colour
	 *            CSS color to use.
	 */
	public void highLight(final By by, final String colour) {
		highLight(driver.findElement(by), colour);
	}

	/**
	 * highlight the element with selected colour.
	 *
	 * @param elem
	 *            element to highlight.
	 * @param colour
	 *            CSS color to use.
	 */
	public void highLight(final WebElement elem, final String colour) {
		if (driver instanceof JavascriptExecutor) {
			((JavascriptExecutor) driver).executeScript("arguments[0].setAttribute('style', arguments[1]);", elem,
					"color: " + colour + "; border: 2px solid " + colour + ";");
		}
	}

	/**
	 * is the web element disabled?
	 *
	 * @param webElement
	 *            webElement
	 */
	protected boolean isDisabled(final WebElement webElement) {
		final String classes = webElement.getAttribute("class");
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("lockbox() - String classes={}", classes); //$NON-NLS-1$
		}
		final boolean contains = GeneralUtils.lookForWholeWord(classes, "disabled");
		collector.checkThat(webElement.getText() + " button should be disabled", !(webElement.isEnabled() || contains),
				equalTo(false));
		return contains;
	}

	/**
	 * is the web element disabled?
	 *
	 * @param webElement
	 *            webElement
	 */
	protected void isEnabled(final WebElement webElement) {
		final String classes = webElement.getAttribute("class");
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("lockbox() - String classes={}", classes); //$NON-NLS-1$
		}
		final boolean contains = GeneralUtils.lookForWholeWord(classes, "disabled");
		collector.checkThat(webElement.getText() + " button should be enabled.", webElement.isEnabled() || contains,
				equalTo(false));
	}

	protected void loadJquery() throws Exception {
		if (!jsonUtils.isjQueryLoaded(driver)) {
			jsonUtils.injectScript(driver, "http://code.jquery.com/jquery-3.2.1.js");
			waitForLoad(5);
			JSWaiter.waitUntilJSReady();
		}
	}

	/**
	 * Log HTML (text) of the elements.
	 *
	 * @param message
	 * @param logger
	 * @param elements
	 */
	protected void logHtml(final String message, final Logger logger, final List<WebElement> elements) {
		if (logger.isInfoEnabled()) {
			logger.info("logHtml(Logger, List<WebElement>) - elements.size={}", elements.size()); //$NON-NLS-1$
			int index = 0;
			for (final WebElement webElement : elements) {
				logger.info("------------- index = " + index++);
				logHtml(message, logger, webElement);
			}
		}
	}

	/**
	 * Log HTML (text) of the elements.
	 *
	 * @param message
	 * @param logger
	 * @param elements
	 */
	protected void logRawHtml(final List<WebElement> elements) {
		for (final WebElement webElement : elements) {
			logRawHtml(webElement);
		}
	}

	/**
	 * Log HTML (text) of the element. (DEBUG AID)
	 *
	 * @param message
	 * @param logger
	 * @param elements
	 */
	protected void logHtml(final String message, final Logger logger, final WebElement webElement) {
		if (logger.isDebugEnabled()) {
			logger.info("logHtml(String, Logger, List<WebElement>) - element={}" + message); //$NON-NLS-1$
			logger.info("logHtml(String, Logger, List<WebElement>) - html=\n{}" + webElement.getAttribute("outerHTML")); //$NON-NLS-1$

			final boolean isDisplayed = webElement.isDisplayed();
			final boolean isEnabled = webElement.isEnabled();
			final boolean isSelected = webElement.isSelected();
			logger.info("logHtml(Logger, WebElement) - isDisplayed={}, isEnabled={}, isSelected={}", isDisplayed, //$NON-NLS-1$
					isEnabled, isSelected);
			try {
				final Point point = webElement.getLocation();
				final Rectangle rectangle = webElement.getRect();
				final Dimension dimension = webElement.getSize();
				logger.debug("logHtml(Logger, WebElement) - point={}, rectangle={}, dimension={}", point, rectangle, //$NON-NLS-1$
						dimension);
			} catch (final Exception e) {
				// do nothing.
			}
		}
	}

	/**
	 * Log HTML (text) of the element. (DEBUG AID)
	 *
	 * @param message
	 * @param logger
	 * @param elements
	 */
	protected void logRawHtml(final WebElement webElement) {
		System.out.println(webElement.getAttribute("outerHTML")); //$NON-NLS-1$
	}

	/**
	 * Return the current URL
	 *
	 * @return the current URL
	 */
	protected String getCurrentUrl() {
		return driver.getCurrentUrl();
	}

	/**
	 * maximize windows.
	 */
	protected void maximize() {
		driver.manage().window().maximize();
	}

	/**
	 * move to the WebElement.
	 *
	 * @param webElement
	 *            to move to.
	 * @return webElement
	 */
	protected WebElement moveTo(final WebElement webElement) {
		new Actions(driver).moveToElement(webElement);
		return webElement;
	}

	/**
	 * Move to the bottom. Not to be confused with
	 * com.mezawadzki.simpleSelenium.common.WebTestBase.scrollToBottom(). This does
	 * not Necessarily change visibility.
	 */
	protected void moveToBottom() {
		((JavascriptExecutor) driver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
	}

	/**
	 * Move to the Top. Not to be confused with
	 * com.mezawadzki.simpleSelenium.common.WebTestBase.scrollToTop(). This does not
	 * Necessarily change visibility.
	 */
	protected void moveToTop() {
		((JavascriptExecutor) driver).executeScript("window.scrollTo(0,0)");
	}

	/**
	 * Scroll to the bottom. Not to be confused with
	 * com.mezawadzki.simpleSelenium.common.WebTestBase.moveToBottom().
	 */
	protected void scrollToBottom() {
		((JavascriptExecutor) driver).executeScript("window.scrollTo(0, document.body.scrollHeight)");
	}

	/**
	 * Scroll to the top. Not to be confused with
	 * com.mezawadzki.simpleSelenium.common.WebTestBase.moveToTop().
	 */
	protected void scrollToTop() {
		((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0)");
	}

	/**
	 * Scroll to the element.
	 *
	 * @param element
	 *            element to scroll to.
	 */
	protected void scrollTo(final WebElement element) {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);
	}

	/**
	 * scroll to coordinate.
	 *
	 * @param x
	 *            x coordinate
	 * @param v
	 *            y coordinate
	 */
	protected void scrollTo(final int x, final int y) {
		((JavascriptExecutor) driver).executeScript("window.scrollBy(" + x + ',' + y + ")");
	}

	/**
	 * move to and click the WebElement.
	 *
	 * @param webElement
	 *            root element.
	 * @by By locator of element to which to move and click
	 * @return webElement
	 */
	protected WebElement moveToAndClick(final By by) {
		return moveToAndClick(findWithWait(by));
	}

	/**
	 * move to and click the WebElement.
	 *
	 * @param webElement
	 *            root element.
	 * @by By locator of element to which to move and click
	 * @return webElement
	 */
	protected WebElement moveToAndClick(final By by, final int timeOut) {
		return moveToAndClick(findWithWait(by), timeOut);
	}

	/**
	 * move to and click the WebElement.
	 *
	 * @param webElement
	 *            to move to and click..
	 * @return webElement
	 */
	protected WebElement moveToAndClick(final WebElement webElement) {
		return moveToAndClick(webElement, Configuration.DEFAULT_TIMEOUT);
	}

	/**
	 * move to and click the WebElement.
	 *
	 * @param webElement
	 *            root element.
	 * @by By locator of element to which to move and click
	 * @return webElement
	 */
	protected WebElement moveToAndClick(final WebElement webElement, final By by) {
		return moveToAndClick(findWithWait(webElement, by));
	}

	/**
	 * move to and click the WebElement.
	 *
	 * @param webElement
	 *            to move to and click..
	 * @return webElement
	 */
	protected WebElement moveToAndClick(final WebElement webElement, final long timeOut) {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("moveToAndClick(WebElement) - start"); //$NON-NLS-1$
		}
		LOGGER.info("Click? " + webElement.getAttribute("outerHTML"));
		try {
			waitForClickable(moveTo(webElement), timeOut).click();
			LOGGER.info("Clicked! " + webElement.getAttribute("outerHTML"));
		} catch (final WebDriverException e) {
			collector.addError(new Throwable("WARNING - Click did not happen  - direct JS click." + e.getMessage()));
			directClick(webElement);
		}
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("moveToAndClick(WebElement) - end"); //$NON-NLS-1$
		}
		return webElement;
	}

	/**
	 * move to <code>by</code> and click. If it Selenium exception then log the
	 * problem, move to <code>byAlternate</code> and send a 'return' char.
	 *
	 * @param by
	 *            clickable element
	 * @param byAlternate
	 *            see above.
	 */
	protected void moveToAndClickWithAlternate(final By by, final By byAlternate, final int timeOut) {
		try {
			moveToAndClick(by, timeOut);
		} catch (final WebDriverException e) {
			LOGGER.warn("moveToAndClickOrReturn(By, By) - exception ignored", e); //$NON-NLS-1$
			collector.addError(new Throwable("WARNING - Problem with click (see log), using alternate. Clickable is "
					+ ReflectionToStringBuilder.toString(by)));
			final WebElement webElement = findWithWait(byAlternate);
			webElement.sendKeys("\n");
		}
	}

	/**
	 * Wait for file to appear.
	 *
	 * @param file
	 *            name of file expected
	 * @param trys
	 *            number of attempts
	 * @param pollEverySeconds
	 *            polling interval.
	 * @return file exists?
	 */
	protected boolean pollFileExists(final File file, final int trys, final int pollEverySeconds) {
		boolean result = false;
		for (int i = 0; i < trys; i++) {
			if (file.exists()) {
				result = true;
				break;
			} else {
				sleep(pollEverySeconds);
			}

		}
		return result;
	}

	/**
	 * scroll to element.
	 *
	 * @param webElement
	 *            webElement
	 */
	protected void scrollToElement(final By by) {
		scrollToElement(driver.findElement(by));
	}

	/**
	 * scroll to element.
	 *
	 * @param webElement
	 *            webElement
	 */
	protected void scrollToElement(final WebElement webElement) {
		if (driver instanceof JavascriptExecutor) {
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", webElement);
		}
	}

	protected WebElement sortButtonElement() {
		return findWithWait(By.xpath("//span[.='Sort by']"));
	}

	/**
	 * reflection to string on ...
	 *
	 * @param object
	 * @return deep toString()
	 */
	protected String toString(final Object object) {
		return ReflectionToStringBuilder.toString(object, ToStringStyle.MULTI_LINE_STYLE);
	}

	protected WebElement waitForClickable(final By by) {
		final WebDriverWait wait = new WebDriverWait(driver, Configuration.DEFAULT_TIMEOUT);
		return wait.until(ExpectedConditions.visibilityOfElementLocated(by));
	}

	protected WebElement waitForClickable(final By by, final long seconds) {
		final WebDriverWait wait = new WebDriverWait(driver, seconds);
		return wait.until(ExpectedConditions.elementToBeClickable(by));
	}

	protected WebElement waitForClickable(final WebElement webElement) {
		final WebDriverWait wait = new WebDriverWait(driver, Configuration.DEFAULT_TIMEOUT);
		return wait.ignoring(StaleElementReferenceException.class)
				.until(ExpectedConditions.elementToBeClickable(webElement));
	}

	protected WebElement waitForClickable(final WebElement webElement, final long seconds) {
		final WebDriverWait wait = new WebDriverWait(driver, seconds);
		return wait.until(ExpectedConditions.visibilityOf(webElement));
	}

	/** wait for dojo to do it's thing */
	public void waitForContentLoad() {
		waitForContentLoad(driver, Configuration.DEFAULT_TIMEOUT, Configuration.DEFAULT_POLLING_EVERY);
	}

	/**
	 * wait for dojo to do it's thing @ param driver - driver to use
	 */
	public void waitForContentLoad(final WebDriver driver) {
		waitForContentLoad(driver, Configuration.DEFAULT_TIMEOUT, Configuration.DEFAULT_POLLING_EVERY);
	}

	/**
	 * wait for Dojo to do it's thing with the spinner.
	 *
	 * @param selenium
	 *            web driver
	 * @param duration
	 *            seconds
	 * @param pollingEvery
	 *            seconds
	 */
	public void waitForContentLoad(final WebDriver driver, final long duration, final long pollingEvery) {
		JSWaiter.waitUntilJSReady();
		waitForElToBeRemove(driver, By.xpath("//img[@src='../../../dojox/widget/Standby/images/loading.gif']"),
				duration, Configuration.DEFAULT_POLLING_EVERY);
		waitForElToBeRemove(driver, By.xpath("//img[@src='/src/dijit/themes/a11y/indeterminate_progress.gif']"),
				duration, Configuration.DEFAULT_POLLING_EVERY);
		waitForElToBeRemove(driver, By.xpath("//img[@src='img/ajax-loader.gif']"), duration,
				Configuration.DEFAULT_POLLING_EVERY);

	}

	private void waitForDojo() {
		JSWaiter.waitDojoReady();
	}

	/**
	 * Wait for element to be removed.
	 *
	 * @param driver
	 * @param by
	 * @param timeout
	 *            in seconds
	 * @return success of wait.
	 */
	protected boolean waitForElToBeRemove(final WebDriver driver, final By by, final long timeout,
			final long pollingEvery) {
		try {
			final Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(timeout, TimeUnit.SECONDS)
					.pollingEvery(pollingEvery, TimeUnit.SECONDS).ignoring(NoSuchElementException.class);
			final boolean present = wait.until(ExpectedConditions.invisibilityOfElementLocated(by));

			return present;
		} catch (final Exception e) {
			return false;
		}
	}

	/**
	 * Wait for Javascript on page to complete.
	 *
	 */
	protected void waitForJSComplete() {
		JSWaiter.waitUntilJSReady();
	}

	/**
	 * wait until the Javascript loads and the spinners spin down. Default wait
	 * duration.
	 *
	 * @param seconds
	 */
	protected void waitForLoad() {
		waitForLoad(WAIT_FOR_LOAD_DURATION_SECONDS);
	}

	/**
	 * wait until the Javascript loads and the spinners spin down.
	 *
	 * @param wait
	 *            duration in seconds.
	 */
	protected void waitForLoad(final Integer seconds) {
		waitForJSComplete();
		waitForContentLoad(driver);
		waitForDojo();
		sleep(seconds);
	}

	protected List<WebElement> waitForNestedPresence(final By parent, final By by) {
		final WebDriverWait wait = new WebDriverWait(driver, Configuration.DEFAULT_TIMEOUT);
		return wait.until(ExpectedConditions.presenceOfNestedElementsLocatedBy(parent, by));
	}

	protected void waitForTableWithRowCount(final By by, final int desiredCount) {
		try {
			final WebDriverWait wait = new WebDriverWait(getDriver(), Configuration.DEFAULT_TIMEOUT);
			wait.until(driver -> {
				final WebElement tblEl = driver.findElement(by);
				final WebElement tbdyEl = tblEl.findElement(By.tagName("tbody"));
				final int elementCount = tbdyEl.findElements(By.tagName("tr")).size();
				if (elementCount == desiredCount) {
					return true;
				} else {
					return false;
				}
			});
		} catch (final TimeoutException e) {
			final String message = String.format("waitForTableWithRowCount(By, int) => '%s', '%s'", by, desiredCount);
			LOGGER.error(message, e); // $NON-NLS-1$
			throw e;
		}
	}

	protected WebElement waitForVisible(final By by) {
		final WebDriverWait wait = new WebDriverWait(driver, Configuration.DEFAULT_TIMEOUT);
		return wait.until(ExpectedConditions.visibilityOfElementLocated(by));
	}

	protected WebElement waitForVisible(final By by, final long seconds) {
		final WebDriverWait wait = new WebDriverWait(driver, seconds);
		return wait.until(ExpectedConditions.visibilityOfElementLocated(by));
	}

	protected WebElement waitForVisible(final WebElement webElement) {
		final WebDriverWait wait = new WebDriverWait(driver, Configuration.DEFAULT_TIMEOUT);
		return wait.until(ExpectedConditions.visibilityOf(webElement));
	}

	protected WebElement waitForVisible(final WebElement webElement, final long seconds) {
		final WebDriverWait wait = new WebDriverWait(driver, seconds);
		return wait.until(ExpectedConditions.visibilityOf(webElement));
	}

	/**
	 * Explicitly wait for element visibility until timeout seconds using the
	 * default timeout.
	 *
	 * @param driver
	 *            WebDriver instance.
	 * @param by
	 *            locator
	 * @return Web Element.
	 */
	protected WebElement waitForVisiblity(final WebDriver driver, final By by) {
		return waitForVisiblity(driver, by, Configuration.DEFAULT_TIMEOUT, Configuration.DEFAULT_POLLING_EVERY);
	}

	/**
	 * Explicitly wait for element visibility until timeout seconds.
	 *
	 * @param driver
	 *            WebDriver instance.
	 * @param by
	 *            locator
	 * @param timeout
	 *            timeout in seconds
	 * @param pollingEvery
	 *            polling interval
	 * @return webelement.
	 */
	protected WebElement waitForVisiblity(final WebDriver driver, final By by, final long timeout,
			final long pollingEvery) {
		final Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(timeout, TimeUnit.SECONDS)
				.pollingEvery(2, TimeUnit.SECONDS)
				.ignoring(NoSuchElementException.class, ElementNotVisibleException.class);
		return wait.until(driver1 -> driver1.findElement(by));
	}

	protected void affirmComboBox(final WebElement localWebElement) {
		// TODO Auto-generated method stub

	}

	protected void affirmRadio(final WebElement localWebElement) {
		// TODO Auto-generated method stub
	}

	protected WebElement filterFormElement() {
		return findWithWait(By.xpath("//input[contains(@id,'dijit_form_FilteringSelect_')]"));
	}

	protected void listAndsearchElements() {
		filterFormElement();
		listSearchWidgetElement();
	}

	private WebElement listSearchWidgetElement() {
		return findWithWait(By.xpath("//input[contains(@id,'ListSearchWidget_')]"));
	}

	/**
	 * Given a webElement check it's label (text) and click it.
	 *
	 * @param By
	 *            target
	 * @param msg
	 *            JUnit message
	 * @param expected
	 *            expected label
	 * @return true if web element exists (is not null) and label is expected, else
	 *         false.
	 */

	protected boolean safeClick(final By by, final String msg, final String expected) {
		return safeClick(driver.findElement(by), msg, expected);
	}

	/**
	 * Given a webElement check it's label (text) and click it.
	 *
	 * @param webElement
	 *            target
	 * @param msg
	 *            JUnit message
	 * @param expected
	 *            expected label
	 * @return true if web element exists (is not null) and label is expected, else
	 *         false.
	 */

	protected boolean safeClick(final WebElement webElement, final String msg, final String expected) {
		boolean result = false;

		// I hate to sleep but it sometimes takes time for the asynch call in
		// the client app to finish their dirty work.
		try {
			Thread.sleep(2000);
		} catch (final InterruptedException e) {
			// nada
		}
		try {
			scrollToElement(webElement);
			waitForVisible(webElement, 15);
			collector.checkThat(webElement.getText(), equalTo(expected));
			webElement.click();

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("safeClick(WebElement, String, String) - successfully clicked - webElement.getText={}", //$NON-NLS-1$
						webElement.getText());
			}

			result = true;
		} catch (NoSuchElementException | TimeoutException | NullPointerException nse) {
			LOGGER.error("No such element/timeout - see log.", nse);
			collector.addError(nse);
		}
		return result;
	}

	protected String quote(final String text) {
		String result = null;
		if (text == null) {
			result = "NULL!";
		} else {
			result = "\"" + StringUtils.defaultIfBlank(text, "") + "\"";
		}
		return result;
	}

	public void goHome() {
		// take me home, country roads to the page I belong.
		if (driver != null) {
			goHome(driver);
		}
	}

	/**
	 * go home after visiting other tabs and panels
	 *
	 * @param driver
	 *            WebDriver.
	 */
	abstract protected void goHome(WebDriver driver);

}
